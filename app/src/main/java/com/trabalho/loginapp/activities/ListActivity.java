package com.trabalho.loginapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;

import java.util.ArrayList;

public class ListActivity extends Activity {

    DatabaseHelper userDatabase;
    ArrayList<String> listUser;
    ArrayAdapter adapter;
    ListView userList;
    EditText userSearch;
    ImageButton buttonSearch, buttonReload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        userList = findViewById(R.id.listView);
        userSearch = findViewById(R.id.listSearch);
        buttonSearch = findViewById(R.id.buttonSearch);
        buttonSearch.bringToFront();
        buttonReload = findViewById(R.id.buttonReload);
        buttonReload.bringToFront();
        userDatabase = new DatabaseHelper(this);
        listUser = new ArrayList<>();
        selectAll();
        selectById();
        setReload();
    }

    public void selectAll() {
        Cursor data = userDatabase.selectAllUsers();
        if (data.getCount() == 0) {
            alert("Nenhum usuário encontrado.");
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        } else {
            while (data.moveToNext()) {
                StringBuilder buffer = new StringBuilder();
                String id = "ID: " + data.getString(0);
                String nome = "\nNome: " + data.getString(1) + "\n\n";
                String telefone = "\nNúmero para contato: " + data.getString(3);
                buffer.append(id).append(nome).append(telefone);
                listUser.add(buffer.toString());
            }
            adapter = new ArrayAdapter<>(this, R.layout.list_white_text, listUser);
            userList.setAdapter(adapter);
        }

    }

    public void selectById() {
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listUser = new ArrayList<>();
                Cursor data = userDatabase.selectUserById(Integer.parseInt(userSearch.getText().toString()));
                while (data.moveToNext()) {
                    StringBuilder buffer = new StringBuilder();
                    String id = "ID: " + data.getString(0);
                    String nome = "\nNome: " + data.getString(1);
                    String email = "\nEmail: " + data.getString(2);
                    String telefone = "\nTelefone: " + data.getString(3);
                    String idade = "\nIdade: " + data.getString(4);
                    buffer.append(id).append(nome).append(email).append(telefone).append(idade);
                    listUser.add(buffer.toString());

                }
                if (listUser.size() != 0) {
                    userList.setAdapter(null);
                    adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.list_white_text, listUser);
                    userList.setAdapter(adapter);
                }
            }
        });
    }

    public void setReload() {
        buttonReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listUser = new ArrayList<>();
                Cursor data = userDatabase.selectAllUsers();
                userList.setAdapter(null);
                while (data.moveToNext()) {
                    StringBuilder buffer = new StringBuilder();
                    String id = "ID: " + data.getString(0);
                    String nome = "\nNome: " + data.getString(1) + "\n\n";
                    String telefone = "\nNúmero para contato: " + data.getString(3);
                    buffer.append(id).append(nome).append(telefone);
                    listUser.add(buffer.toString());
                }
                adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.list_white_text, listUser);
                userList.setAdapter(adapter);
            }
        });
    }

    public void alert(String alert) {
        Toast.makeText(ListActivity.this, alert, Toast.LENGTH_SHORT).show();
    }
}
