package com.trabalho.loginapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;

public class ConfigurationActivity extends Activity {

    DatabaseHelper userDatabase;
    EditText editAdmName, editAdmConfirmPassword, getAdmCurrentPassword, editAdmNewPassword;
    String admName, passwordCompare;
    Button buttonConfirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        userDatabase = new DatabaseHelper(this);
        buttonConfirmar = findViewById(R.id.buttonConfirmar);
        editAdmName = findViewById(R.id.userConfigurationNome);
        getAdmName();
        getButtonConfirmar();
    }

    public void getAdmName() {
        Cursor data = userDatabase.selectAllAdms();
        while (data.moveToNext()) {
            admName = data.getString(1);
        }
        Log.i("admname", "" + admName);
        editAdmName.setText(admName);
    }

    public void getButtonConfirmar() {
        buttonConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome, senha, senhaAtual, novaSenha, confirmarSenha;

                getAdmCurrentPassword = findViewById(R.id.configurationCurrentPassword);
                senhaAtual = getAdmCurrentPassword.getText().toString();

                editAdmNewPassword = findViewById(R.id.configurationNewPassword);
                novaSenha = editAdmNewPassword.getText().toString();

                editAdmConfirmPassword = findViewById(R.id.configurationConfirmNewPassword);
                confirmarSenha = editAdmConfirmPassword.getText().toString();

                Cursor data = userDatabase.selectAllAdms();
                while (data.moveToNext()) {
                    passwordCompare = data.getString(2);
                }
                if (senhaAtual.equals(passwordCompare) && novaSenha.equals(confirmarSenha) && !novaSenha.isEmpty() && !senhaAtual.isEmpty()) {
                    nome = editAdmName.getText().toString();
                    senha = novaSenha;
                    boolean updateAdm = userDatabase.updateAdm("1", nome, senha);
                    if (updateAdm) {
                        alert("Modificado com sucesso!");
                        Intent intent = new Intent(ConfigurationActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    } else {
                        alert("Não foi possível modificar.");
                    }
                } else {
                    alert("Preencha os campos corretamente.");
                }
            }
        });
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
