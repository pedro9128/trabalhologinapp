package com.trabalho.loginapp.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;
import com.trabalho.loginapp.tools.LifeCycle;

public class MainActivity extends LifeCycle {

    TextView textView;
    DatabaseHelper userDatabase;
    String passwordCompare, userCompare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.LabelPin);
        userDatabase = new DatabaseHelper(this);
        clickPin();
        Button button = findViewById(R.id.buttonEntrar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextUser = findViewById(R.id.userField);
                String user = editTextUser.getText().toString();

                EditText editTextSenha = findViewById(R.id.passwordField);
                String senha = editTextSenha.getText().toString();
                Cursor data = userDatabase.selectAllAdms();
                if (data.getCount() == 0) {
                    alert("Não existem registros.");
                } else
                    while (data.moveToNext()) {
                        userCompare = data.getString(1);
                        passwordCompare = data.getString(2);
                    }
                if (user.equals(userCompare) && senha.equals(passwordCompare)) {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    intent.putExtra("text", user);
                    startActivity(intent);
                } else {
                    alert("Usuário ou senha incorreto(a)!");
                }

            }
        });
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getContext() {
        return this;
    }

    public void clickPin() {
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor data = userDatabase.selectAllAdms();
                if (data.getCount() == 0) {
                    Intent intent = new Intent(MainActivity.this, PinActivity.class);
                    startActivity(intent);
                } else {
                    alert("Já cadastrado no sistema!");
                }
            }
        });
    }
}
