package com.trabalho.loginapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;

public class UpdateActivity extends Activity {

    DatabaseHelper userDatabase;
    Button buttonUpdate;
    EditText editId, editNome, editEmail, editTelefone, editIdade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        userDatabase = new DatabaseHelper(this);
        buttonUpdate = findViewById(R.id.buttonUpdate);
        editId = findViewById(R.id.updateId);
        editNome = findViewById(R.id.updateNome);
        editEmail = findViewById(R.id.updateEmail);
        editTelefone = findViewById(R.id.updateTelefone);
        editIdade = findViewById(R.id.updateIdade);
        getUser();
    }

    public void onButtonUpdateClick() {
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = editId.getText().toString();
                String nome = editNome.getText().toString();
                String email = editEmail.getText().toString();
                String telefone = editTelefone.getText().toString();
                int idade = Integer.parseInt(editIdade.getText().toString());

                boolean updateUser = userDatabase.updateUser(id, nome, email, telefone, idade);
                if (updateUser)
                    alert("Usuário atualizado com sucesso!");
                else
                    alert("Não foi possível atualizar usuário.");

                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
    }

    public void alert(String alert) {
        Toast.makeText(UpdateActivity.this, alert, Toast.LENGTH_SHORT).show();
    }

    public void getUser() {
        Cursor data = userDatabase.selectAllUsers();
        if (data.getCount() == 0) {
            alert("Nenhum usuário encontrado.");
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        } else
            onButtonUpdateClick();
    }
}
