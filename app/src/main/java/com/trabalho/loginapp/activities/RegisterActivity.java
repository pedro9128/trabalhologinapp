package com.trabalho.loginapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;
import com.trabalho.loginapp.tools.LifeCycle;


public class RegisterActivity extends LifeCycle {

    DatabaseHelper userDatabase;

    EditText editNome, editEmail, editTelefone, editIdade;
    Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        userDatabase = new DatabaseHelper(this);

        editNome = findViewById(R.id.registerNome);
        editEmail = findViewById(R.id.registerEmail);
        editTelefone = findViewById(R.id.registerTelefone);
        editIdade = findViewById(R.id.registerIdade);
        buttonRegister = findViewById(R.id.buttonRegister);

        insertUser();
    }

    public void insertUser() {
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome = editNome.getText().toString();
                String email = editEmail.getText().toString();
                String telefone = editTelefone.getText().toString();
                int idade = Integer.parseInt(editIdade.getText().toString());

                boolean insertUser = userDatabase.addUser(nome, email, telefone, idade);

                if (insertUser)
                    alert("Usuário inserido com sucesso!");
                else
                    alert("Não foi possível inserir usuário.");

                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
    }

    public void alert(String alert) {
        Toast.makeText(RegisterActivity.this, alert, Toast.LENGTH_SHORT).show();
    }
}
