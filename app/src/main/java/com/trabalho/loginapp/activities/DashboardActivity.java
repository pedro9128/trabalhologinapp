package com.trabalho.loginapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;

public class DashboardActivity extends Activity implements View.OnClickListener {

    Button btn, btn1, btn2, btn3;
    String userLogin;
    DatabaseHelper userDatabase;
    TextView textViewUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        btn = findViewById(R.id.iconCad);
        btn1 = findViewById(R.id.iconListar);
        btn2 = findViewById(R.id.iconEditar);
        btn3 = findViewById(R.id.iconConfig);
        userDatabase = new DatabaseHelper(this);
        welcomeMessage();
        btn.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iconCad:
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.iconListar:
                intent = new Intent(getApplicationContext(), ListActivity.class);
                startActivity(intent);
                break;
            case R.id.iconEditar:
                intent = new Intent(getApplicationContext(), UpdateActivity.class);
                startActivity(intent);
                break;
            case R.id.iconConfig:
                intent = new Intent(DashboardActivity.this, ConfigurationActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void welcomeMessage() {
        textViewUserName = findViewById(R.id.userName);
        Cursor data = userDatabase.selectAllAdms();
        while (data.moveToNext()) {
            userLogin = data.getString(1);
        }
        textViewUserName.setText(userLogin);
    }
}
