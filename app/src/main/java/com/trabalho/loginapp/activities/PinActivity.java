package com.trabalho.loginapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trabalho.loginapp.R;
import com.trabalho.loginapp.tools.DatabaseHelper;

import java.util.ArrayList;

public class PinActivity extends Activity {
    String pin;
    Button button;
    EditText textPin, textPassword;
    ArrayList<String> text;
    DatabaseHelper userDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pin = "0603";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        textPassword = findViewById(R.id.pinPassword);
        userDatabase = new DatabaseHelper(this);
        button = findViewById(R.id.buttonPin);
        text = new ArrayList<>();
        onClickButton();
    }

    public void onClickButton() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getPin, nome;
                nome = "amaury123";
                textPin = findViewById(R.id.pinField);
                getPin = textPin.getText().toString();
                if (getPin.equals(pin)) {
                    String senha = textPassword.getText().toString();
                    boolean insertAdm = userDatabase.addAdm(nome, senha);
                    if (insertAdm) {
                        alert("Registrado com sucesso!");
                        Intent intent = new Intent(PinActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        alert("Não foi possível registrar.");
                    }

                } else
                    alert("Pin incorreto! Tente novamente.");
            }
        });
    }


    public void alert(String alert) {
        Toast.makeText(this, alert, Toast.LENGTH_SHORT).show();
    }
}
