package com.trabalho.loginapp.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "register";
    private static final String TABLE_NAME_USR = "user_login";
    private static final String TABLE_NAME_ADM = "adm_login";
    private static final String COL_ID_USR = "id";
    private static final String COL_NOME_USR = "nome";
    private static final String COL_EMAIL_USR = "email";
    private static final String COL_TELEFONE_USR = "telefone";
    private static final String COL_IDADE_USR = "idade";
    private static final String COL_ID_ADM = "id";
    private static final String COL_NOME_ADM = "nome";
    private static final String COL_SENHA_ADM = "senha";

    private static final String CREATE_TABLE_USR = "CREATE TABLE " + TABLE_NAME_USR +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, email TEXT, telefone TEXT, idade INTEGER)";

    private static final String CREATE_TABLE_ADM = "CREATE TABLE " + TABLE_NAME_ADM +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, senha TEXT)";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_USR);
        db.execSQL(CREATE_TABLE_ADM);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_USR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ADM);
        onCreate(db);
    }

    public boolean addUser(String nome, String email, String telefone, int idade) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NOME_USR, nome);
        contentValues.put(COL_EMAIL_USR, email);
        contentValues.put(COL_TELEFONE_USR, telefone);
        contentValues.put(COL_IDADE_USR, idade);

        long result = db.insert(TABLE_NAME_USR, null, contentValues);

        return result != -1;
    }

    public boolean updateUser(String id, String nome, String email, String telefone, int idade) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_ID_USR, id);
        contentValues.put(COL_NOME_USR, nome);
        contentValues.put(COL_EMAIL_USR, email);
        contentValues.put(COL_TELEFONE_USR, telefone);
        contentValues.put(COL_IDADE_USR, idade);
        db.update(TABLE_NAME_USR, contentValues, COL_ID_USR + "= ?", new String[]{id});
        return true;
    }

    public boolean addAdm(String nome, String senha) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NOME_ADM, nome);
        contentValues.put(COL_SENHA_ADM, senha);

        long result = db.insert(TABLE_NAME_ADM, null, contentValues);

        return result != -1;
    }

    public boolean updateAdm(String id, String nome, String senha) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_ID_ADM, id);
        contentValues.put(COL_NOME_USR, nome);
        contentValues.put(COL_SENHA_ADM, senha);
        db.update(TABLE_NAME_ADM, contentValues, COL_ID_ADM + "= ?", new String[]{id});
        return true;
    }

    public Cursor selectAllUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME_USR, null);
    }

    public Cursor selectUserById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME_USR + " WHERE id= " + id, null);
    }

    public Cursor selectAllAdms() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME_ADM, null);
    }

    public Cursor selectAdmByName(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_NAME_ADM + " WHERE nome = " + name, null);
    }
}
